# My Team Fortress 2 Configs #

### Install ###

1. Clone or download this repository
2. Find your cfg directory:
    1. Open `Steam.exe`
    2. Go to the **Library** tab
    3. Right click **Team Fortess 2** in the list of games
    4. Then **Managed** > **Browse local files** (and File Explorer should open)
    5. In File Explorer, go to **tf** > **cfg**
3. Move the desired files from this repository into the cfg directory

### Usage ###

Unless you are myself, I highly discourage anyone from copying all of the files into their cfg directory.
Many of these configurations are **highly** opinionated.
I would treat this similar to hand-me-downs.
You might like your cousin's shirt, but you wouldn't want to wear their underwear.
Pick and choose what you'd like, but use at your own discretion.
My personal recommendation would be to copy over the following:
- `autoexec.cfg`
- `demoman.cfg`
- `engineer.cfg`
- `heavyweapons.cfg`
- `medic.cfg`
- `pyro.cfg`
- `scout.cfg`
- `sniper.cfg`
- `soldier.cfg`
- `spy.cfg`

The rest of my configurations are in `config.cfg`, but I have not detailed what I use in there.
It is mostly tailored to my setup specifically.
So if you'd like to look through it, be my guest.
But please **do not** blindly copy this into your cfg directory.

#### Class Configurations ####

Each corresponding configuration is executed in game once the player has switched to that class.
For example, when you switch to scout, the `scout.cfg` file will run by the tf2 client.
You do not need to restart your game or anything special.
This is a built-in feature of the tf2 game to run scripts named by their class.
Though, with the exception to the naming convention being heavy's script called `heavyweapons.cfg`.

Note: each class configuration runs the `autoexec.cfg` script which contains commands that are ran no matter which class you use.
You can think of this like global configurations.

#### Optional Scipts ####

Some `.cfg` files are specifically to be executed on certain maps (usually for practicing in a private server) to make them operational or to control the bots in some way.
Map scripts are named by the same corresponding map name.
Bot scripts are usually specific to that map and are named similarly.

My personal favorite maps to install being `tr_walkway` and `tr_newbot`.

### Uninstall ###

Given that you've read the above, I'm going to assume you've only copied over a few optional scripts, the class specific scripts, and the `autoexe.cfg` script.
This means you only have to delete those files to revert back to your old settings.
You can refer to the thorough list in the **Usage** section for the class specific scripts.
There's no need to edit or alter your settings or other configurations to uninstall.

To simply deactivate the config but keep it, you can simply rename the file to something else.

### Overview ###

#### General Keybindings ####

- same movement with WASD, ctrl, space, and mouse
- same m1 and m2 behavior
- scroll wheel scrolls only between weapons 1,2,3
- q goes to last weapon
- Q drops item
- keys 7,8,9,0 correspond to loadouts 1,2,3,4
- f1 thru f12 row is unmodified
- t,y,u are global, party, and team text chat respectively
- z,x,c calls for medic, spy, and says thanks respectively
- Z,X,C opens voice_menu 1,2,3 respectively
- v starts voice chat
- V calls for help (b/c voice is more important than calling help)
- escape key still cancels menus
- tab key still shows the score
- back tick still shows the dev console
- m shows map info
- , and . change class and team respectively
- < and > show loadouts and inventory respectively
- p toggles first and third person (if server allows behavior)
- n (unstuck) fixes the weapon stuck bug
- - and = trigger and declice notifs respectively
- r reloads and R inspects
- f uses action item slot and F taunts
- backspace and delete keys both kys

#### Engineer Configs ####

- e and m3 destroys last sentry and builds a new one
- 4 destroys last dispenser and builds a new one
- 5 destroys last teleporter entrance and builds a new one
- 6 destroys last teleporter exit and builds a new one
- b and B teleports to spawn and the teleporter exit respectively with the eureka (one of the engineer's wrenches)

#### Spy Configs ####

- 1 is gun (default)
- 2 is sapper (default)
- 3 is knife (default)
- 4 disguise kit (default)
- m3 disguise kit
- e last disguise (akin to q for last weapon)
- r disguise team

#### Misc ####

- fov and player model changed
- networking upgrades
- performance upgrades
- hit digs are set and given max volume
- medic autocallers
- medic autoheal (so you don't have to hold down m1 to heal)
- medic heal target marker
- minimal gui
- simple disguise menu
- [hud fastswitch](https://developer.valvesoftware.com/wiki/Hud_fastswitch)
- developer console enabled
- disabled motion blur
- sniper full charge bell
- adjust anti-aliasing
- closecaptions